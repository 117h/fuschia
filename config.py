import os

# Statement for enabling the development environment
DEBUG = True

# Define the application directory
BASE_DIR = os.path.abspath(os.path.dirname(__file__))

# Define the database - we are working with
# SQLite for this example
SQLALCHEMY_DATABASE_URI = 'postgresql://musicbrainz:music@localhost:5432/musicbrainz'
DATABASE_CONNECT_OPTIONS = {}

THREADS_PER_PAGE = 8

# encryption key to avoid CSRF. For flaks-WTForms
SECRET_KEY = 'fuschiamusic'

