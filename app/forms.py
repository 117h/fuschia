from flask.ext.wtf import Form
from wtforms import StringField, SubmitField, PasswordField
from wtforms.validators import InputRequired, Email, EqualTo, DataRequired

from app.models import User


class SignUpForm(Form):
    name = StringField(label='Name', validators=[InputRequired('Name is Required')])
    email = StringField(label='Email', validators=[InputRequired('Email is Required'), Email('Not a Valid Email')])
    password = PasswordField(label='Password', validators=[InputRequired('Password is Required')])
    submit = SubmitField(label='Sign Up')

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)
        self.user = None

    def validate(self):
        rv = Form.validate(self)
        if not rv:
            return False

        # user = User(user=self.username.data, email='mist', sex='m', age=12)
        user = User.query.filter_by(email=self.email.data).first()

        if user is not None:
            error = 'Email already in use'
            self.email.errors.append(error)
            print("ERROR: %s" % error)
            return False

        user = User(
               name=self.name.data,
               email=self.email.data,
               sex='m',
               age=21)
        user.password = self.password.data

        self.user = user
        return True


class SignInForm(Form):
    email = StringField(label='Email', validators=[InputRequired(), Email()])
    password = PasswordField(label='Password', validators=[InputRequired()])
    submit = SubmitField(label='Submit')

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)
        self.user = None

    def validate(self):
        rv = Form.validate(self)
        if not rv:
            return False

        user = User.query.filter_by(email=self.email.data).first()
        if user is None:
            error = 'Account not present'
            self.email.errors.append(error)
            print("ERROR: %s" % error)
            return False

        if not user.verify_password(self.password.data):
            error = 'Wrong password'
            self.password.errors.append(error)
            print("ERROR: %s" % error)
            return False

        self.user = user
        return True


class SettingsForm(Form):
    old_password = PasswordField(label='Old Password', validators=[InputRequired()])
    new_password = PasswordField(label='New Password', validators=[InputRequired()])
    confirm_password = PasswordField(label='Confirm Password', validators=[DataRequired(), EqualTo(new_password)])
