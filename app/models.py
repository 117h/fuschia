from app import db, login_manager
from datetime import datetime

from werkzeug.security import generate_password_hash, check_password_hash
from flask.ext.login import UserMixin


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class User(UserMixin, db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(50), unique = True, index = True)
    email = db.Column(db.String(100), nullable = False, unique= True, index = True)
    sex = db.Column(db.String(10))
    age = db.Column(db.Integer)
    password_hash = db.Column(db.String(128))


    def __init__(self, name, email, sex, age):
        self.name = name
        self.email = email
        self.sex = sex
        self.age = age

    def __repr__(self):
        return '<User %s, Email %s>' % (self.name, self.email)


    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)




class Transaction(db.Model):
    __tablename__ = 'transactions'

    #tid = db.Column(db.Integer, primary_key = True)

    id = db.Column(db.Integer, primary_key = True)
    uid = db.Column(db.Integer, db.ForeignKey('users.id'))
    song_id = db.Column(db.Integer)
    time = db.Column(db.DateTime, default = datetime.now)
    ip = db.Column(db.String(100))
    mode = db.Column(db.Integer) #1 = searched by user,  2 = present in playlist,  3 = recommended

    def __init__(self, uid, song_id, time = datetime.now, ip="192.168.35.5", mode=0):
        self.uid = uid
        self.song_id = song_id
        self.time = time
        self.ip = ip
        self.mode = mode

    def __repr__(self):
        return "<Transactions(uid='%d', song_id='%d)>" % (self.uid, self.song_id)


class Playlist(db.Model):
    __tablename__ = 'playlists'

    #pid = db.Column(db.Integer, primary_key = True)

    id = db.Column(db.Integer, primary_key = True)
    uid = db.Column(db.Integer, db.ForeignKey('users.id'))
    name = db.Column(db.String(50))
    song_id = db.Column(db.Integer)
    serial = db.Column(db.Integer)

    def __init__(self, uid, name, song_id, serial):
        self.uid = uid
        self.name = name
        self.song_id = song_id
        self.serial = serial

    def __repr__(self):
        return '<Playlists %d>' % self.uid


class Interest(db.Model): #initialised by user on sign-up/login
    __tablename__ = 'interests'

    id = db.Column(db.Integer, primary_key = True)
    uid = db.Column(db.Integer, db.ForeignKey('users.id'))
    genre = db.Column(db.String(50))
    rel = db.relationship("User", backref = db.backref('interest'))

