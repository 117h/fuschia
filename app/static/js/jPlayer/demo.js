$(document).ready(function(){

  var myPlaylist = new jPlayerPlaylist({
    jPlayer: "#jplayer_N",
    cssSelectorAncestor: "#jp_container_N"
  }, [
  ], {
    playlistOptions: {
      enableRemoveControls: true,
      autoPlay: false
    },
    swfPath: "js/jPlayer",
    supplied: "webmv, ogv, m4v, oga, mp3",
    smoothPlayBar: true,
    keyEnabled: true,
    audioFullScreen: false
  });
  
  var mp3_link = "http://dl.blugaa.com/lq.blugaa.com/cdn6/7a7a628afc8a1aff04995b64cb191d51/iulsv/Tera%20Mera%20Rishta%20%20Awarapan%20%20-(Mr-Jatt.com).mp3"

  $(document).on($.jPlayer.event.pause, myPlaylist.cssSelector.jPlayer,  function(){
    $('.musicbar').removeClass('animate');
    $('.jp-play-me').removeClass('active');
    $('.jp-play-me').parent('li').removeClass('active');
  });

  $(document).on($.jPlayer.event.play, myPlaylist.cssSelector.jPlayer,  function(){
    $('.musicbar').addClass('animate');
  });

  $(document).on('click', '.jp-play-me', function(e){ e && e.preventDefault();
    var $this = $(e.target);
    if (!$this.is('a')) $this = $this.closest('a');

    $('.jp-play-me').not($this).removeClass('active');
    $('.jp-play-me').parent('li').not($this.parent('li')).removeClass('active');

    var meta_container = $this.siblings('.track-meta')
    console.log(meta_container)
    var track_name = meta_container.children('.meta-track-name')[0].innerText
    console.log(track_name)
    var track_artist = meta_container.children('.meta-track-artist')[0].innerText
    //var track_id = meta_container.children('.meta-track-id')[0].innerText
    var track_id = 10
    //var track_cover = meta_container.children('.meta-track-cover')[0].innerText

    console.log("Playing song")

    $this.toggleClass('active');
    $this.parent('li').toggleClass('active');
    if( !$this.hasClass('active') ){
      myPlaylist.pause();
    }else{
      myPlaylist.remove();
      console.log("Executing function")
      $.getJSON('/_get_track', {
        user_id: 100,
        track_id: track_id
      }, function(data) {
        myPlaylist.pause();
        myPlaylist.add({
          title: track_name,
          artist: track_artist,
          mp3: data['mp3'],
          poster: "images/m0.jpg"
        });
        myPlaylist.play(0);
      });
    }
    
  });

});