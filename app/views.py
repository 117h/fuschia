from flask import render_template, url_for, redirect, request, flash, session, jsonify
from app import app, db
from app.models import User, Transaction
from flask.ext.login import login_required, logout_user, login_user

from app.forms import SignUpForm, SignInForm, SettingsForm


######################
# REQUEST DECORATORS #
######################



###############
# AJAX ROUTES #
###############



@app.route('/_get_track')
def get_track():
    mp3_link1 = "http://dl.blugaa.com/lq.blugaa.com/cdn6/7a7a628afc8a1aff04995b64cb191d51/iulsv/Tera%20Mera%20Rishta%20%20Awarapan%20%20-(Mr-Jatt.com).mp3"
    mp3_link2 = "http://skymaza.com/upload_file/1/3647/3663/3665/Linkin%20Park%20-%20Hybrid%20Theory%20-%2008%20-%20In%20The%20End-.mp3"
    import random
    user_id = request.args.get('user_id', 0, type=int)
    track_id = request.args.get('track_id', 0, type=int)
    print("User id: %d, song id: %d" % (user_id, track_id))
    if(random.randint(1, 2) == 1):
        return jsonify(mp3=mp3_link1)
    else:
        return jsonify(mp3=mp3_link2)


####################
# ROUTE DECORATORS #
####################

@app.route('/')
@login_required
def index():
    return redirect(url_for('home'))

@app.route('/home')
@login_required
def home():
    topsongs = [{'id': 12, 'cover': "images/a1.png", 'name': "Lose Yourself", 'artist': "Eminem"},
                {'id': 12, 'cover': "images/a1.png", 'name': "Till I Collapse", 'artist': "Eminem"},
                {'id': 12, 'cover': "images/a1.png", 'name': "Backdrift", 'artist': "Radiohead"},
                {'id': 12, 'cover': "images/a1.png", 'name': "Deeper Underground", 'artist': "Jamiroquai"}]
    newsongs = [{'id': 12, 'cover': "images/a1.png", 'name': "Lose Yourself", 'artist': "Eminem"},
                {'id': 12, 'cover': "images/a1.png", 'name': "Till I Collapse", 'artist': "Eminem"},
                {'id': 12, 'cover': "images/a1.png", 'name': "Backdrift", 'artist': "Radiohead"},
                {'id': 12, 'cover': "images/a1.png", 'name': "Deeper Underground", 'artist': "Jamiroquai"}]
    recordings =[{'id': 12, 'cover': "images/a1.png", 'name': "Lose Yourself", 'artist': "Eminem", 'stars': 4},
                 {'id': 12, 'cover': "images/a1.png", 'name': "Till I Collapse", 'artist': "Eminem", 'stars': 3},
                 {'id': 12, 'cover': "images/a1.png", 'name': "Backdrift", 'artist': "Radiohead", 'stars': 2},
                 {'id': 12, 'cover': "images/a1.png", 'name': "Deeper Underground", 'artist': "Jamiroquai", 'stars': 4},
                 {'id': 12, 'cover': "images/a1.png", 'name': "Lose Yourself", 'artist': "Eminem", 'stars': 4},
                 {'id': 12, 'cover': "images/a1.png", 'name': "Till I Collapse", 'artist': "Eminem", 'stars': 3},
                 {'id': 12, 'cover': "images/a1.png", 'name': "Backdrift", 'artist': "Radiohead", 'stars': 2},
                 {'id': 12, 'cover': "images/a1.png", 'name': "Zombie", 'artist': "The Cranberries", 'stars': 4},
                 {'id': 12, 'cover': "images/a1.png", 'name': "In the End", 'artist': "Linkin Park", 'stars': 3},
                 {'id': 12, 'cover': "images/a1.png", 'name': "Faint", 'artist': "Linkin Park", 'stars': 3},
                 {'id': 12, 'cover': "images/a1.png", 'name': "Teardrops", 'artist': "Massive Attack", 'stars': 5},
                 {'id': 12, 'cover': "images/a1.png", 'name': "Say It Right", 'artist': "Nelly Furtado", 'stars': 2}]
        # instead retrieve these variables from the database. don't know how.
    return render_template('home.html', topsongs=topsongs, newsongs=newsongs, recordings=recordings)

@app.route('/listen')
@login_required
def listen():
    recommended_tracks = [{'id': 13, 'cover': "images/a1.png", 'name': "Lose Yourself", 'artist': "Eminem"},
                          {'id': 13, 'cover': "images/a1.png", 'name': "Till I Collapse", 'artist': "Eminem"},
                          {'id': 13, 'cover': "images/a1.png", 'name': "Backdrift", 'artist': "Radiohead"},
                          {'id': 13, 'cover': "images/a1.png", 'name': "Deeper Underground", 'artist': "Jamiroquai"}]
    return render_template('listen.html', recommended_tracks=recommended_tracks)

@app.route('/genres')
@login_required
def genres():
    return render_template('genres.html')


@app.route('/signin', methods=['GET', 'POST'])
def signin():
    form = SignInForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user is not None and user.verify_password(form.password.data):
            login_user(user)
            return redirect(request.args.get('next') or url_for('home'))
    flash('Invalid email or password.')
    print('Invalid email or password.')
    return render_template('signin.html', body_class="bg-info dker", form=form)

@app.route('/signup', methods=['GET', 'POST'])
def signup():
    form = SignUpForm()
    if form.validate_on_submit():
        # enter user into database
        db.session.add(form.user)
        db.session.commit()
        return redirect(url_for('signin'))

    return render_template('signup.html', body_class="bg-info dker", form=form)


@app.route('/settings')
@login_required
def settings():
    return render_template('settings.html')

@app.route('/signout')
@login_required
def signout():
    logout_user()
    flash("You have been logged out")
    print("User logged out")
    return redirect(url_for('signin'))


# errors
@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

@app.errorhandler(500)
def internal_server_error(e):
    return render_template('500.html'), 500
