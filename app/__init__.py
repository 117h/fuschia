import os
import sys

from flask import Flask, render_template
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.login import LoginManager


app = Flask(__name__)
app.config.from_object('config')

login_manager = LoginManager(app)
login_manager.session_protection = 'strong'
login_manager.login_view = 'signin'

db = SQLAlchemy(app)
