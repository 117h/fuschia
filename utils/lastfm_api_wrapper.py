import requests

JSON_URL = 'http://ws.audioscrobbler.com/2.0/?method=track.getInfo&api_key={api_key}&mbid={mbid}&user={user}&format=json'

class LastFM:

    def __init__(self, api_key=None, user=None):
        """
        Initialize the class with your api credentials.
        :param api_key: API key related with your account
        :param user: username of lastfm API account
        """

        self.api_key = api_key
        self.user = user

    def __repr__(self):
        print("Last FM API wrapper. api_key = %s. by altairmn." % self.api_key)


    def get_meta_by_mbid(self, mbid):
        """
        This method requires the mbid of track and returns
        a dictionary with track info retrieved from last.fm
        :param mbid: musicbrainz id of track
        """
        req_str = JSON_URL.format(api_key=self.api_key, mbid=mbid, user=self.user)
        req = requests.get(req_str)
        return req.json()

# Example code:
#
#     from lastfm_api_wrapper import LastFM
#
#     lastfm = LastFM(api_key=YOUR_API_KEY, user=YOUR_USER_NAME)
#     track_metadata = lastfm.get_meta_by_mbid(mbid=TRACK_MBID)
#
#     print(track_metadata)
