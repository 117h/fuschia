
class User(object):

    def __init__(self, name):
        print('initializing object...')
        self.name = name
        self.password_hash = 100

    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        self.password_hash = 10



